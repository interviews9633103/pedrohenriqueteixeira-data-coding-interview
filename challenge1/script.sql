
select 
	sched_dep_time,
	case when LENGTH(sched_dep_time::varchar(255)) = 4 then substring(sched_dep_time::varchar(255), 1, 2) || ':' || substring(sched_dep_time::varchar(255), 3, 4)
	else substring(sched_dep_time::varchar(255), 1, 1) || ':' || substring(sched_dep_time::varchar(255), 2,3)
	end,
	f.origin,
	w.origin,
	w.humid as humid_origin,
	f.dest
from flights f
inner join weather w on f.origin  = w.origin 
